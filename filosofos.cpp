#include <ncurses.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <pthread.h>
#include <ctime>
using namespace std;

int N_FILOS, MAX_ESTOMAGO = 100;
int comida = ((unsigned int) -1) >> 1;
int restauraciones = 0;

pthread_mutex_t *flag, flagComida;

WINDOW ** frames;

void* filosofo(void *arg){
	int id = *( (int *) arg );
	int nextLock = (id+1)%N_FILOS;
	int estomago = 10, haComido = 0, haPensado=0;

 /*
  +---------------+
  |  Filosofo 22  |// (1, 3)
  |Estomago:   22 |// (2, 1) +3 esp 
  |Ha comido:  22 |// (3, 1) +2 esp
  |Ha pensado: 22 |// (4, 1) +1 esp
  |   PENSANDO    |// +3 esp (5, 4)
  |   COMIENDO    |//
  mvwprintw(frames[i], 1, 3, "Filosofo %d", i);
   */

	while(true){
		//PENSANDO
		--estomago; ++haPensado;
		mvwprintw(frames[id], 2, 1, "Estomago:   %d", estomago);
		mvwprintw(frames[id], 4, 1, "Ha pensado: %d", haPensado);
		wattron(frames[id], A_REVERSE);
		mvwprintw(frames[id], 5, 4, "   PENSANDO");
		wattroff(frames[id], A_REVERSE);
		wrefresh(frames[id]);
		usleep(rand()&1048575);	
		if (estomago >= MAX_ESTOMAGO){
			continue;
		}
		
		//COMIENDO
		pthread_mutex_lock(&flag[id]);
		pthread_mutex_lock(&flag[nextLock]);
		
		pthread_mutex_lock(&flagComida);
		--comida;
		if (comida <= 0){
      comida = ((unsigned int) -1) >> 1;
      ++restauraciones;
    }		
		pthread_mutex_unlock(&flagComida);

		++estomago; ++haComido;
		mvwprintw(frames[id], 2, 1, "Estomago:   %d", estomago);
		mvwprintw(frames[id], 3, 1, "Ha comido:  %d", haComido);
		wattron(frames[id], A_REVERSE);
		mvwprintw(frames[id], 5, 4, "   COMIENDO");
		wattroff(frames[id], A_REVERSE);
		usleep(rand()&2097151);
		pthread_mutex_unlock(&flag[id]);
		pthread_mutex_unlock(&flag[nextLock]);
	}

	pthread_exit(NULL);
}

int main(int argc, char* argv[]){
	srand(time(NULL));
	initscr();
	raw();
	cbreak();
	noecho();

	start_color();

	int h, w;
	getmaxyx(stdscr, h, w);

	/*
 +---------------+
 |  Filosofo 22  |// 11+4+2 (1, 3)
 |Estomago:   22 |//
 |Ha comido:  22 |// (3, 1)
 |Ha pensado: 22 |//
 |   PENSANDO    |// (5, 4)
 |   COMIENDO    |//
	//Altura 4+2
	*/

	N_FILOS = atoi(argv[1]);
	int id[N_FILOS];
	flag = new pthread_mutex_t[N_FILOS];	
	frames = new WINDOW* [N_FILOS];
	int i;	
	pthread_t filosofos[N_FILOS];

	for(i = 0; i < N_FILOS; ++i){
		int maxColum = w/19;
		
		pthread_mutex_init(&flag[i], NULL);
		id[i] = i;
		/*
		0 1 2 3 4  0
		5 6 7 8 9  1
		A B C D E  2
		*/
		frames[i] = newwin(6, 17, 1+i/maxColum*6, 2+i%maxColum*17);
		printw("%d-%d ", 1+i/maxColum*6, 1+i%maxColum*17);
		refresh();
		box(frames[i], 0, 0);
		mvwprintw(frames[i], 1, 3, "Filosofo %d", i);
		wrefresh(frames[i]);
	}
	refresh();
	for(i = 0; i < N_FILOS; ++i){
		//fflush(stdout);
		pthread_create(&filosofos[i], NULL, &filosofo, &id[i]);
	}
	//JOIN
	for(i = 0; i < N_FILOS; ++i){
		pthread_join(filosofos[i], NULL);
		pthread_mutex_destroy(&flag[i]);
	}
	
	return 0;
}
